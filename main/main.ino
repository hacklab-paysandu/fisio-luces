#include "Modulo.h"

// Apagado en orden de encendido o al revés
// Encienden todos a la vez y apagar en el menor tiempi
// Mostrar todos los colores e indicar cual es el correcto y tener que apagar ese
// Lo mismo que el anterior pero con una palabra que diga un color, y aparezca de otro, el correcto es el color con el que la palabra esta pintada

#define CANT_MODULOS  4

Color colores[CANT_MODULOS];
Modulo modulos[CANT_MODULOS];

#define MODO_TODOS      1
#define MODO_SECUENCIA  2
#define MODO_3          3
#define MODO_COLOR      4

#define PIN_BT_5V       8
#define PIN_BT_DISC     9

int modo = 0;
unsigned long tiempos[CANT_MODULOS];
int rnd[CANT_MODULOS];
int correcto = 0;
int iApagado = 0;

#define LOG Serial
#define COM Serial1

void encenderBT ()
{
  LOG.println( "Desconectando BT" );
  COM.end();
  digitalWrite( PIN_BT_5V, LOW );
  delay( 1000 );
  digitalWrite( PIN_BT_5V, HIGH );
  COM.begin( 9600 );
  while ( !COM );
  
  if ( COM && COM.available() ) COM.read();
  
  LOG.println( "BT Conectado!" );
}

void setup ()
{  
  // Iniciar comunicación
  LOG.begin( 9600 );

  // Esperar por si aún no se ha establecido conexión
  while( !LOG );
  
  // Setear colores por defecto
  colores[0].set( 255, 0, 0 );
  colores[1].set( 0, 255, 0 );
  colores[2].set( 0, 0, 255 );
  colores[3].set( 255, 255, 255 );

  // Inicializar modulos, con sus pines
  modulos[0].iniciar( 3, 52, 50, 48, true );
  modulos[1].iniciar( 2, 44, 42, 40, false );
  modulos[2].iniciar( 20, 36, 34, 32, false );
  modulos[3].iniciar( 21, 28, 25, 24, false );

  // Setear interrupts para cuando se accione un módulo
  attachInterrupt( digitalPinToInterrupt( modulos[0].getSensor() ), sensor0, RISING );
  attachInterrupt( digitalPinToInterrupt( modulos[1].getSensor() ), sensor1, RISING );
  attachInterrupt( digitalPinToInterrupt( modulos[2].getSensor() ), sensor2,  RISING );
  attachInterrupt( digitalPinToInterrupt( modulos[3].getSensor() ), sensor3, RISING );

  // Iniciar todos los módulos apaga                                                                      dos
  for ( byte i = 0; i < CANT_MODULOS; i++ )
  {
    modulos[i].apagar();
    rnd[i] = i; // Iniciar orden random, con las id de los colores
  }

  pinMode( PIN_BT_DISC, INPUT );
  pinMode( PIN_BT_5V, OUTPUT );
  encenderBT();

  randomSeed( analogRead( 0 ) );
}

void reiniciar ()
{
  modo = 0;
  correcto = 0;
  iApagado = 0;

  encenderTodos( Color::getColor( 0, 0, 0 ) );
}

String comando = "";
void loop ()
{
  // Sí llegó un mensaje
  if ( COM && COM.available() )
  {
    // Se lee el siguiente caracter
    char c = COM.read();

    // Si el caracter era una línea nueva, entonces el mensaje ya llegó por completo
    if ( c == '\n' )
    {
      LOG.println( comando );
      
      // Se chequea qué opción es y se llama a la función correcta
      // Estas funciones están en el archivo 'opciones'
      if ( comando.indexOf( "set_color" ) != -1 )
        set_color( delFirst( comando ) );
      else if ( comando.indexOf( "modo" ) != -1 )
      {
        String modo = delFirst( comando );

        if ( modo.indexOf( "todos_i" ) != -1 )
          todos_i();
        else if ( modo.indexOf( "todos_rnd" ) != -1 )
          todos_rnd();
        else if ( modo.indexOf( "todos_color_i" ) != -1 )
          todos_color_i( delFirst( modo ) );
        else if ( modo.indexOf( "todos_color_s" ) != -1 )
          todos_color_s( delFirst( modo ) );
        else if ( modo.indexOf( "sec" ) != -1 )
          sec( delFirst( modo ) );
        else if ( modo.indexOf( "color" ) != -1 )
          modo_color( delFirst( modo ) );
        else if ( modo.indexOf( "palabra" ) != -1 )
          modo_palabra( delFirst( modo ) );
      }
      else if ( comando.indexOf( "fin" ) != -1 )
      {
        reiniciar();
      }
      
      comando = "";
    }
    else // De lo contrario, aún quedan caracteres por recibir
      comando += c;
  }
}

void sensor0 () { sensor( 0 ); }
void sensor1 () { sensor( 1 ); }
void sensor2 () { sensor( 2 ); }
void sensor3 () { sensor( 3 ); }
