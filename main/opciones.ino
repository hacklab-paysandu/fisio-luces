/* Cambia el color de la posicion i, por los valores
 * 
 * i: index del color
 * r: rojo
 * g: verde
 * b: azul
 * */
void set_color ( String args )
{
  int i, r, g, b;
  args > i > r > g > b;

  colores[i].set( r, g, b );
}

/* Por cada módulo del arreglo modulos, enciende el modulo[i] con el color[i]
 * */
void todos_i ()
{
  if ( modo != 0 )
    return;
  
  int p = random( CANT_MODULOS );
  encenderTodos( colores[p] );
  
  modo = MODO_TODOS;
}

/* Enciende todos los módulos con distintos colores de los especificados
 * */
void todos_rnd ()
{
  // TODO: Aleatorio
  COM.println( "todos_rnd" );

  for ( byte i = 0; i < CANT_MODULOS; i++ )
  {
    int p = random( CANT_MODULOS );
    int tmp = rnd[p];
    rnd[p] = rnd[i];
    rnd[i] = tmp;
  }

  for ( byte i = 0; i < CANT_MODULOS; i++ )
    encender( i, colores[rnd[i]] );
  
  modo = MODO_TODOS;
}

/* Enciende todos los módulos con el color i
 * 
 * i: index del color a setear
 * */
void todos_color_i ( String args )
{
  if ( modo != 0 )
    return;
  
  int i;
  args > i;
  
  encenderTodos( colores[i] );
  
  modo = MODO_TODOS;
}

/* Enciende todos los colores al color indicado por parametro
 * 
 * r: rojo
 * g: verde
 * b: azul
 * */
void todos_color_s ( String args )
{
  if ( modo != 0 )
    return;
  
  int r, g, b;
  args > r > g > b;
  
  encenderTodos( Color::getColor( r, g, b ) );
  
  modo = MODO_TODOS;
}

void sec ( String args )
{
  if ( modo != 0 )
    return;
  
  for ( byte i = 0; i < CANT_MODULOS; i++ )
  {
    int p = random( CANT_MODULOS );
    int tmp = rnd[p];
    rnd[p] = rnd[i];
    rnd[i] = tmp;
  }

  for ( byte i = 0; i < CANT_MODULOS; i++ )
  {
    encender( rnd[i], colores[rnd[i]] );
    delay( 1000 );
    encender( rnd[i], Color::getColor( 0, 0, 0 ) );
  }
  
  for ( byte i = 0; i < CANT_MODULOS; i++ )
    encender( rnd[i], colores[rnd[i]] );

  iApagado = 0;
  modo = MODO_SECUENCIA;
}

void modo_color ( String args )
{
  int col;
  args > col;
  
  for ( byte i = 0; i < CANT_MODULOS; i++ )
  {
    int p = random( CANT_MODULOS );
    int tmp = rnd[p];
    rnd[p] = rnd[i];
    rnd[i] = tmp;
  }
  
  for ( byte i = 0; i < CANT_MODULOS; i++ )
  {
    encender( rnd[i], colores[i] );

    if ( i == col )
      correcto = rnd[i];
  }

  modo = MODO_COLOR;
}

void modo_palabra ( String args )
{
  
}
