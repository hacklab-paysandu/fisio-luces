#include "Color.h"

class Modulo
{
  private:
    int pinR;
    int pinG;
    int pinB;
    int pinSensor;
    bool encendido;
    bool anodo;

  public:
    void iniciar  ( int, int, int, int, bool );
    void color    ( Color );
    void apagar   ();
    int getSensor ();
    bool estaEncendido ();
};
