#include "Modulo.h"
#include <Arduino.h>

void Modulo::iniciar ( int pinSensor, int pinR, int pinG, int pinB, bool anodo )
{
  this->pinSensor = pinSensor;
  
  this->pinR = pinR;
  this->pinG = pinG;
  this->pinB = pinB;

  this->anodo = anodo;

  pinMode( pinSensor, INPUT );

  pinMode( pinR, OUTPUT );
  pinMode( pinG, OUTPUT );
  pinMode( pinB, OUTPUT );
}

void Modulo::color ( Color c )
{
  if ( !anodo )
  {
    analogWrite( pinR, c.getR() );
    analogWrite( pinG, c.getG() );
    analogWrite( pinB, c.getB() );
  }
  else
  {
    analogWrite( pinR, 255 - c.getR() );
    analogWrite( pinG, 255 - c.getG() );
    analogWrite( pinB, 255 - c.getB() );
  }

  encendido = true;
}

void Modulo::apagar ()
{
  color( Color::getColor( 0, 0, 0 ) );
  encendido = false;
}

int Modulo::getSensor ()
{
  return pinSensor;
}

bool Modulo::estaEncendido ()
{
  return encendido;
}
