#include "Color.h"

Color::Color () {}

Color::Color ( int r, int g, int b ) { set( r, g, b ); }

void Color::set ( int r, int g, int b )
{
  this->r = r;
  this->g = g;
  this->b = b;
}

int Color::getR () { return this->r; }
int Color::getG () { return this->g; }
int Color::getB () { return this->b; }

Color Color::getColor ( int r, int g, int b ) { return Color( r, g, b ); }
