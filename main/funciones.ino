void sensor ( int i )
{
  // Si el módulo ya estaba apagado que no haga nada
  if ( modo == 0 || !modulos[i].estaEncendido() )
    return;

  // Se apaga el módulo
  modulos[i].apagar();

  // Se envía el tiempo que el módulo paso encendido
  COM.print( "apagado:" );
  COM.print( i );
  COM.print( ":" );
  COM.print( millis() - tiempos[i] );

  if ( modo == MODO_TODOS )
    COM.println( "" );
  
  bool termino = false;
  if ( modo == MODO_TODOS || modo == MODO_SECUENCIA )
  {
    bool apagados = true;
    for ( byte i = 0; i < CANT_MODULOS; i++ )
      if ( modulos[i].estaEncendido() )
      {
        apagados = false;
        break;
      }

    if ( apagados )
      termino = true;
  }
  if ( modo == MODO_SECUENCIA )
  {
    if ( rnd[iApagado] == i )
      COM.println( ":c" );
    else
      COM.println( ":i" );
    
    iApagado++;
  }
  else if ( modo == MODO_COLOR )
  {
    if ( i == correcto )
    {
      COM.println( ":c" );
      encenderTodos( Color::getColor( 0, 0, 0 ) );
      termino = true;
    }
    else
      COM.println( ":i" );
  }
  
  if ( termino )
  {
    COM.println( "fin" ); 
    modo = 0;
  }
}

void encenderTodos ( Color c )
{
  for ( byte i = 0; i < CANT_MODULOS; i++ )
    encender( i, c );
}

void encender ( int i, Color c )
{
  modulos[i].color( c );
  tiempos[i] = millis();
}
