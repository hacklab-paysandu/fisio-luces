class Color
{
  private:
    int r;
    int g;
    int b;

  public:
    Color ();
    Color ( int, int, int );

    void set( int, int, int );
    int getR();
    int getG();
    int getB();

    static Color getColor( int, int, int );
};
