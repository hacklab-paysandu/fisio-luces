#define ROJO        2
#define AZUL        3
#define VERDE       4
#define AMARILLO    5

#define B_ROJO      9
#define B_AZUL      10
#define B_VERDE     11
#define B_AMARILLO  12

#define VOLTAJE     13

bool jugando = false;

char juego;

char orden[4];
char resultado[4];
bool encendidos[4];
int i = 0;

unsigned long tiempoInicio = 0;
unsigned long tiempoAnterior = 0;
long tiempos[4];

void setup ()
{
  Serial.begin( 9600 );
  
  pinMode( ROJO, OUTPUT );
  pinMode( AZUL, OUTPUT );
  pinMode( VERDE, OUTPUT );
  pinMode( AMARILLO, OUTPUT );
  
  pinMode( VOLTAJE, OUTPUT );
  
  pinMode( B_ROJO, INPUT );
  pinMode( B_AZUL, INPUT );
  pinMode( B_VERDE, INPUT );
  pinMode( B_AMARILLO, INPUT );

  digitalWrite( ROJO, LOW );
  digitalWrite( AZUL, LOW );
  digitalWrite( VERDE, LOW );
  digitalWrite( AMARILLO, LOW );
  
  digitalWrite( VOLTAJE, HIGH );

  orden[0] = '1';
  orden[1] = '2';
  orden[2] = '3';
  orden[3] = '4';

  randomSeed( analogRead( 0 ) );
}

bool test = false;

void loop ()
{
  if ( test )
  {
    secuencia();
  
    digitalWrite( ROJO, HIGH );
    digitalWrite( AZUL, HIGH );
    digitalWrite( VERDE, HIGH );
    digitalWrite( AMARILLO, HIGH );
  
    delay( 2000 );
    
    digitalWrite( ROJO, LOW );
    digitalWrite( AZUL, LOW );
    digitalWrite( VERDE, LOW );
    digitalWrite( AMARILLO, LOW );
    
    return;
  }
  
  if ( Serial.available() )
  {
    char c = Serial.read();

    if ( c == '\n' || jugando )
      return;

    jugando = true;
    juego = c;

    encendidos[0] = encendidos[1] = encendidos[2] = encendidos[3] = false;
    tiempos[0] = tiempos[1] = tiempos[2] = tiempos[3] = 0;
    tiempoAnterior = 0;

    i = 0;
    
    if ( c == '0' )
    {
      todosActivos();
    }
    else if ( c == '1' )
    {
      secuencia();
    }
    
    tiempoInicio = millis();
    i = 0;
  }

  if ( !jugando )
    return;

  if ( digitalRead( B_ROJO ) && !encendidos[0] )
  {
    digitalWrite( ROJO, LOW );
    encendidos[0] = true;

    resultado[i] = '1';
    mostrarInfo( '1' );
    i++;
  }
  else if ( digitalRead( B_AZUL ) && !encendidos[1] )
  {
    digitalWrite( AZUL, LOW );
    encendidos[1] = true;
    
    resultado[i] = '2';
    mostrarInfo( '2' );
    i++;
  }
  else if ( digitalRead( B_VERDE ) && !encendidos[2] )
  {
    digitalWrite( VERDE, LOW );
    encendidos[2] = true;
    
    resultado[i] = '3';
    mostrarInfo( '3' );
    i++;
  }
  else if ( digitalRead( B_AMARILLO ) && !encendidos[3] )
  {
    digitalWrite( AMARILLO, LOW );
    encendidos[3] = true;
    
    resultado[i] = '4';
    mostrarInfo( '4' );
    i++;
  }

  if ( i == 4 )
  {
    jugando = false;
    i = 0;

    Serial.println( "----------" );
    
    Serial.print( "Tiempo total: " );
    Serial.print( ( float ) ( tiempoAnterior - tiempoInicio ) / 1000 );
    Serial.println( " s" );
    
    Serial.println( "==========" );
  }
}

void todosActivos ()
{
  Serial.println( "TODOS" );
  Serial.println( "----------" );
  
  digitalWrite( ROJO, HIGH );
  digitalWrite( AZUL, HIGH );
  digitalWrite( VERDE, HIGH );
  digitalWrite( AMARILLO, HIGH );
}

void secuencia ()
{
  if ( !test )
  {
    Serial.println( "SECUENCIA" );
    Serial.println( "----------" );
  }
  
  randomOrden();
  
  for ( int j = 0; j < 4; j++ )
  {
    digitalWrite( ROJO, LOW );
    digitalWrite( AZUL, LOW );
    digitalWrite( VERDE, LOW );
    digitalWrite( AMARILLO, LOW );
    
    if ( orden[j] == '1' )
    {
      digitalWrite( ROJO, HIGH );
      delay( 1000 );
    }
    else if ( orden[j] == '2' )
    {
      digitalWrite( AZUL, HIGH );
      delay( 1000 );
    }
    else if ( orden[j] == '3' )
    {
      digitalWrite( VERDE, HIGH );
      delay( 1000 );
    }
    else if ( orden[j] == '4' )
    {
      digitalWrite( AMARILLO, HIGH );
      delay( 1000 );
    }
  }
  
  digitalWrite( ROJO, HIGH );
  digitalWrite( AZUL, HIGH );
  digitalWrite( VERDE, HIGH );
  digitalWrite( AMARILLO, HIGH );
}

void mostrarInfo ( char led )
{ 
  tiempos[i] = millis() - ( i == 0 ? tiempoInicio : tiempoAnterior );
  tiempoAnterior = ( i == 0 ? tiempoInicio : tiempoAnterior ) + tiempos[i];
 
  Serial.print( "Tiempo " );
  Serial.print( i + 1 );
  Serial.print( ": " );
  Serial.print( ( float ) tiempos[i] / 1000 );
  Serial.print( " s " );
  Serial.println( juego == '1' ? ( led == orden[i] ? "CORRECTO" : "INCORRECTO" ) : "" );
}

void randomOrden ()
{
  for ( int i = 0; i < 4; i++ )
  {
    int n = random( 0, 4 );
    char t = orden[n];
    orden[n] = orden[i];
    orden[i] = t;
  }
}
