#include "Modulo.h"
#include <Arduino.h>

void Modulo::iniciar ( int pinSensor, int pinR, int pinG, int pinB )
{
  this->pinSensor = pinSensor;
  
  this->pinR = pinR;
  this->pinG = pinG;
  this->pinB = pinB;

  pinMode( pinSensor, INPUT );

  pinMode( pinR, OUTPUT );
  pinMode( pinG, OUTPUT );
  pinMode( pinB, OUTPUT );
}

void Modulo::color ( int red, int green, int blue )
{
  analogWrite( pinR, red );
  analogWrite( pinG, green );
  analogWrite( pinB, blue );
}

void Modulo::apagar ()
{
  color( 0, 0, 0 );
}

int Modulo::getSensor ()
{
  return pinSensor;
}
