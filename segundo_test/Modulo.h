class Modulo
{
  private:
    int pinR;
    int pinG;
    int pinB;
    int pinSensor;

  public:
    void iniciar  ( int, int, int, int );
    void color    ( int, int, int );
    void apagar   ();
    int getSensor ();
};
