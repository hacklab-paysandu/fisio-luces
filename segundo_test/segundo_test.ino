#include "Modulo.h"

// Apagado en orden de encendido o al revés
// Encienden todos a la vez y apagar en el menor tiempi
// Mostrar todos los colores e indicar cual es el correcto y tener que apagar ese
// Lo mismo que el anterior pero con una palabra que diga un color, y aparezca de otro, el correcto es el color con el que la palabra esta pintada

#define CANT_MODULOS  3
Modulo modulos[CANT_MODULOS];
bool encendido[CANT_MODULOS];
long iniciado[CANT_MODULOS];

void setup ()
{
  Serial.begin( 9600 );

  modulos[0].iniciar( 2, 53, 51, 49 );
  modulos[1].iniciar( 3, 45, 43, 41 );
  modulos[2].iniciar( 20, 37, 35, 33 );

  attachInterrupt( digitalPinToInterrupt( modulos[0].getSensor() ), sensor0, RISING );
  attachInterrupt( digitalPinToInterrupt( modulos[1].getSensor() ), sensor1, RISING );
  attachInterrupt( digitalPinToInterrupt( modulos[2].getSensor() ), sensor2, RISING );
  // attachInterrupt( digitalPinToInterrupt( modulos[3].getSensor() ), sensor3, RISING );
  
  for ( byte i = 0; i < CANT_MODULOS; i++ )
  {
    modulos[i].apagar();
    encendido[i] = false;
    iniciado[i] = 0;
  }
  
  while( !Serial );
}

String comando = "";
void loop ()
{
  if ( Serial.available() )
  {
    char c = Serial.read();

    if ( c == '\n' )
    {
      if ( comando.indexOf( "modo" ) != -1 )
      {
        String modo = delFirst( comando );
      
        if ( modo.indexOf( "todos" ) != -1 )
        {
          Serial.println( "TODOS" );
        }
        else if ( modo.indexOf( "secuencia" ) != -1 )
        {
          Serial.println( "SECUENCIA" );
        }
        else if ( modo.indexOf( "uno_correcto" ) != -1 )
        {
          int i, c;
          delFirst( modo ) < i < c;
          Serial.print( "UNO CORRECTO " );
          Serial.print( i );
          Serial.print( " " );
          Serial.println( c );
        }
      }
      else if ( comando == "apagar:todo" )
        for ( byte i = 0; i < CANT_MODULOS; i++ )
          modulos[i].apagar();
      else if ( comando.indexOf( "apagar" ) != -1 )
      {
        int i;
        delFirst( comando ) < i;
        modulos[i].apagar();
      }
      else if ( comando.indexOf( "colorTodos" ) != -1 )
      {
        int r, g, b;
        delFirst( comando ) < r < g < b;
        for ( byte i = 0; i < CANT_MODULOS; i++ )
          encender( i, r, g, b );
      }
      else if ( comando.indexOf( "color" ) != -1 )
      {
        int i, r, g, b;
        delFirst( comando ) < i < r < g < b;
        encender( i, r, g, b );
      }
      
      comando = "";
    }
    else
      comando += c;
  }
}

void encenderTodos ( int r, int g, int b )
{
  for ( byte i = 0; i < CANT_MODULOS; i++ )
    encender( i, r, g, b );
}

void encender ( int i, int r, int g, int b )
{
  modulos[i].color( r, g, b );
  encendido[i] = true;
  iniciado[i] = millis();
}

void sensor ( int i )
{
  if ( encendido[i] )
  {
    long tiempo = millis() - iniciado[i];
    
    Serial.print( i );
    Serial.print( ":" );
    Serial.println( tiempo );
    
    modulos[i].apagar();
    encendido[i] = false;
  }
}

void sensor0 () { sensor( 0 ); }
void sensor1 () { sensor( 1 ); }
void sensor2 () { sensor( 2 ); }
void sensor3 () { sensor( 3 ); }
